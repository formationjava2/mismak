package mismak.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Produit {
	private int id;
	private String reference;
	private String libelle;
	private BigDecimal prixHT;
	private boolean archive;
	
	public static final BigDecimal TVA = new BigDecimal("20.0");
	
	public Produit() {
		super();
	}
	
	public Produit(String reference, String libelle, BigDecimal prixHT) {
		super();
		this.reference = reference;
		this.libelle = libelle;
		this.prixHT = prixHT;
		this.archive = false;
	}
	
	public Produit(String reference, String libelle, BigDecimal prixHT, boolean archive) {
		super();
		this.reference = reference;
		this.libelle = libelle;
		this.prixHT = prixHT;
		this.archive = archive;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public BigDecimal getPrixHT() {
		return prixHT;
	}

	public void setPrixHT(BigDecimal prixHT) {
		this.prixHT = prixHT;
	}

	public boolean isArchive() {
		return archive;
	}

	public void setArchive(boolean archive) {
		this.archive = archive;
	}
	
	public BigDecimal getPrixTTC() {
		return this.prixHT.multiply(TVA.divide(new BigDecimal(100)).add(BigDecimal.ONE)).setScale(2, RoundingMode.HALF_EVEN);
	}

	@Override
	public String toString() {
		return reference + " - " + libelle + " - " + prixHT + "�";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produit other = (Produit) obj;
		if (id != other.id)
			return false;
		return true;
	}	
}
