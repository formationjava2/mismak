package mismak.model;

import java.util.ArrayList;

import mismak.dao.impl.LignesCommandeDAOImpl;
import mismak.dao.model.LignesCommandeDAO;

public class CollectionLignesCommande {

	private ArrayList<LigneCommande> listeLignesCommande;
	private Commande commande;
	
	private LignesCommandeDAO dao;
	
	public CollectionLignesCommande(Commande commande) {
		this.commande = commande;
		this.dao = new LignesCommandeDAOImpl();
		this.refresh();
	}
	
	public boolean ajouter(LigneCommande ligneCommande) {
		if(this.dao.create(ligneCommande)) {
			this.refresh();
			return true;
		}
		else
			return false;
	}
	
	public boolean modifier(LigneCommande ligneCommande) {
		if(this.dao.update(ligneCommande)) {
			this.refresh();
			return true;
		}
		else
			return false;
	}
	
	public boolean supprimer(LigneCommande ligneCommande) {
		if(this.dao.delete(ligneCommande)) {
			this.refresh();
			return true;
		}
		else
			return false;
	}
		
	private void refresh() {
		if(this.listeLignesCommande == null)
			this.listeLignesCommande = new ArrayList<LigneCommande>();
		
		this.listeLignesCommande.clear();
		this.listeLignesCommande.addAll(this.dao.findByCommandeId(this.commande.getId()));
	}

	public ArrayList<LigneCommande> getListeLignesCommande() {
		return listeLignesCommande;
	}

	public Commande getCommande() {
		return commande;
	}
}
