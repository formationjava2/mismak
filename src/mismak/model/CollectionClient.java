package mismak.model;

import java.util.ArrayList;

import mismak.dao.impl.ClientDAOImpl;
import mismak.dao.impl.CommandeDAOImpl;
import mismak.dao.model.ClientDAO;
import mismak.dao.model.CommandeDAO;

public class CollectionClient {

	private boolean selectedArchive;
	private ArrayList<Client> listeClients;
	private ClientDAO dao;
	
	public CollectionClient() {
		this.dao = new ClientDAOImpl();
		this.selectedArchive = false;
		this.refresh();
	}
	
	public boolean ajouter(Client client) {
		if(this.dao.create(client)) {
			this.refresh();
			return true;
		}
		else
			return false;
	}
	
	public boolean modifier(Client client) {
		if(this.dao.update(client)) {
			this.refresh();
			return true;
		}
		else
			return false;
	}
	
	public boolean supprimer(Client client) {
		CommandeDAO commandeDAO = new CommandeDAOImpl();
		if(commandeDAO.findByClient(client, false).size() > 0)
			return this.setArchive(client, true);
		else if(this.dao.delete(client)) {
			this.refresh();
			return true;
		}
		else
			return false;
	}
		
	public boolean setArchive(Client client, boolean archive) {
		client.setArchive(archive);
		return this.modifier(client);
	}
		
	private void refresh() {
		if(listeClients == null)
			this.listeClients = new ArrayList<Client>();
		
		this.listeClients.clear();
		this.listeClients.addAll(this.dao.findAll(this.selectedArchive));
	}

	public ArrayList<Client> getListeClients() {
		return listeClients;
	}

	public void setSelectedArchive(boolean selectedArchive) {
		this.selectedArchive = selectedArchive;
		this.refresh();
	}

	public boolean isSelectedArchive() {
		return selectedArchive;
	}

	
}
