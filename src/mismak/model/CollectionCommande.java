package mismak.model;

import java.util.ArrayList;

import mismak.dao.impl.CommandeDAOImpl;
import mismak.dao.model.CommandeDAO;

public class CollectionCommande {

	private ArrayList<Commande> listeCommandes;
	private CommandeDAO dao;
	private Client client; // Client dont on veut afficer les commandes
	private Integer idCommandeRecherche;
	
	public CollectionCommande() {
		this.dao = new CommandeDAOImpl();
		this.refresh();
	}
	
	public CollectionCommande(Client client) {
		this.dao = new CommandeDAOImpl();
		this.client = client;
		this.refresh();
	}
	
	public boolean ajouter(Commande commande) {
		if(this.dao.create(commande)) {
			this.refresh();
			return true;
		}
		else
			return false;
	}
	
	public boolean modifier(Commande commande) {
		if(this.dao.update(commande)) {
			this.refresh();
			return true;
		}
		else
			return false;
	}
	
	public boolean supprimer(Commande commande) {
		if(this.dao.delete(commande)) {
			this.refresh();
			return true;
		}
		else
			return false;
	}
		
	public void refresh() {
		if(listeCommandes == null)
			this.listeCommandes = new ArrayList<Commande>();
		
		this.listeCommandes.clear();
		if(idCommandeRecherche != null)
			this.listeCommandes.add(this.client != null ? this.dao.findByClientAndCommandeId(this.client, this.idCommandeRecherche, true) : this.dao.findByCommandeId(this.idCommandeRecherche, true));
		else
			this.listeCommandes.addAll(this.client != null ? this.dao.findByClient(this.client, true) : this.dao.findAll(true));
	}

	public ArrayList<Commande> getListeCommandes() {
		return listeCommandes;
	}
	
	public boolean setPaye(Commande commande) {
		commande.setPaye(true);
		return this.modifier(commande);
	}

	public void rechercher(Integer idCommandeRecherche) {
		this.idCommandeRecherche = idCommandeRecherche;
		this.refresh();		
	}
}
