package mismak.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

public class Commande {

	public static final BigDecimal TVA = new BigDecimal("20.0");
	
	private int id;
	private Date date;
	private boolean paye;
	private Client client;
	private BigDecimal totalHT;
		
	public Commande(Date date, Client client, boolean paye) {
		super();
		this.date = date;
		this.client = client;
		this.paye = paye;
	}
	
	public Commande() {
		super();
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isPaye() {
		return paye;
	}

	public void setPaye(boolean paye) {
		this.paye = paye;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public BigDecimal getTotalHT() {
		return totalHT;
	}
	
	public BigDecimal getTotalTTC() {
		if(totalHT != null)
			return totalHT.multiply(TVA.divide(new BigDecimal(100)).add(BigDecimal.ONE)).setScale(2, RoundingMode.HALF_EVEN);
		else
			return null;
	}

	public void setTotalHT(BigDecimal totalHT) {
		this.totalHT = totalHT;
	}
}
