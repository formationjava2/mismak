package mismak.model;

import java.util.ArrayList;

import mismak.dao.impl.CommandeDAOImpl;
import mismak.dao.impl.ProduitDAOImpl;
import mismak.dao.model.CommandeDAO;
import mismak.dao.model.ProduitDAO;

public class CollectionProduit {

	private boolean selectedArchive;
	private ArrayList<Produit> listeProduits;
	private ProduitDAO dao;
	
	public CollectionProduit() {
		this.dao = new ProduitDAOImpl();
		this.selectedArchive = false;
		this.refresh();
	}
	
	public boolean ajouter(Produit produit) {
		if(this.dao.create(produit)) {
			this.refresh();
			return true;
		}
		else
			return false;
	}
	
	public boolean modifier(Produit produit) {
		if(this.dao.update(produit)) {
			this.refresh();
			return true;
		}
		else
			return false;
	}
	
	public boolean supprimer(Produit produit) {
		CommandeDAO commandeDAO = new CommandeDAOImpl();
		if(commandeDAO.findByProduit(produit).size() > 0)
			return this.setArchive(produit, true);
		else if(this.dao.delete(produit)) {
			this.refresh();
			return true;
		}
		else
			return false;
	}
		
	public boolean setArchive(Produit produit, boolean archive) {
		produit.setArchive(archive);
		return this.modifier(produit);
	}
		
	private void refresh() {
		if(listeProduits == null)
			this.listeProduits = new ArrayList<Produit>();
		
		this.listeProduits.clear();
		this.listeProduits.addAll(this.dao.findAll(this.selectedArchive));
	}

	public ArrayList<Produit> getListeProduits() {
		return this.listeProduits;
	}

	public void setSelectedArchive(boolean selectedArchive) {
		this.selectedArchive = selectedArchive;
		this.refresh();
	}

	public boolean isSelectedArchive() {
		return selectedArchive;
	}

	
}
