package mismak.pdf;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;

import mismak.dao.impl.ClientDAOImpl;
import mismak.dao.impl.CommandeDAOImpl;
import mismak.dao.impl.ProduitDAOImpl;
import mismak.dao.model.ClientDAO;
import mismak.dao.model.CommandeDAO;
import mismak.dao.model.ProduitDAO;
import mismak.model.Client;
import mismak.model.Produit;

public class Statistiques {
	
	private final static int TOP = 10;

	public static String genererPDF() {
		// R�cup�ration des statistiques et cr�ation du fichier de stats
		DateFormat fileDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();  
        
		PdfWriter pdfWriter;
		String fileName = "stats_" + fileDateFormat.format(date) + ".pdf";

		try {
			pdfWriter = new PdfWriter(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}

		final PdfDocument pdfDocument = new PdfDocument(pdfWriter);
		final Document document = new Document(pdfDocument, PageSize.A4);
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat heureFormat = new SimpleDateFormat("HH:mm");
		
		Paragraph titre = new Paragraph("Statistiques du " + dateFormat.format(date) + " � " + heureFormat.format(date)).setFontSize(20.0f)
				.setTextAlignment(TextAlignment.CENTER);
		document.add(titre);

		// CA Total
		caTotal(document);
		
		// Statistiques sur les produits
		produitsPlusVendus(document);
		produitsMoinsVendus(document);
				
		// CA par client
		caParClient(document);
				
		document.close();
		return fileName;
	}
	
	private static void caParClient(Document document) {
		ClientDAO clientDAO = new ClientDAOImpl();
		
		Map<Client, BigDecimal> resultats = clientDAO.getCAParClient();
		
		float[] columnWidths = {80, 20};
		
		document.add(new Paragraph("Chiffre d'affaire par client").setBold().setFontSize(14.0f));
		
		Table table = new Table(UnitValue.createPercentArray(columnWidths)).useAllAvailableWidth();
		table.addCell(new Cell().add(new Paragraph("Client")).setTextAlignment(TextAlignment.CENTER).setBold());
		table.addCell(new Cell().add(new Paragraph("CA")).setTextAlignment(TextAlignment.CENTER).setBold());
		
		for (Map.Entry<Client, BigDecimal> ca : resultats.entrySet()) {
			table.addCell(ca.getKey().toString());
			table.addCell(ca.getValue() + " �");
	    }
		
		document.add(table);
	}
	
	private static void produitsMoinsVendus(Document document) {
		ProduitDAO produitDAO = new ProduitDAOImpl();
		Map<Produit, Integer> resultats = produitDAO.getMoinsVendus(TOP);
		
		float[] columnWidths = {80, 20};
		
		document.add(new Paragraph("Top " + TOP + " des produits les moins vendus").setBold().setFontSize(14.0f));

		Table table = new Table(UnitValue.createPercentArray(columnWidths)).useAllAvailableWidth();
		table.addCell(new Cell().add(new Paragraph("Produit")).setTextAlignment(TextAlignment.CENTER).setBold());
		table.addCell(new Cell().add(new Paragraph("Qt�")).setTextAlignment(TextAlignment.CENTER).setBold());
		
		for (Map.Entry<Produit, Integer> venteProduit : resultats.entrySet()) {
			table.addCell(venteProduit.getKey().toString());
			table.addCell(venteProduit.getValue().toString());
	    }
		
		document.add(table);
	}

	private static void produitsPlusVendus(Document document) {
		ProduitDAO produitDAO = new ProduitDAOImpl();
		Map<Produit, Integer> resultats = produitDAO.getPlusVendus(TOP);
		
		float[] columnWidths = {80, 20};
		
		document.add(new Paragraph("Top " + TOP + " des produits les plus vendus").setBold().setFontSize(14.0f));

		Table table = new Table(UnitValue.createPercentArray(columnWidths)).useAllAvailableWidth();
		table.addCell(new Cell().add(new Paragraph("Produit")).setTextAlignment(TextAlignment.CENTER).setBold());
		table.addCell(new Cell().add(new Paragraph("Qt�")).setTextAlignment(TextAlignment.CENTER).setBold());
		
		for (Map.Entry<Produit, Integer> venteProduit : resultats.entrySet()) {
			table.addCell(venteProduit.getKey().toString());
			table.addCell(venteProduit.getValue().toString());
		}
		
		document.add(table);		
	}
	
	private static void caTotal(Document document) {
		CommandeDAO commandeDAO = new CommandeDAOImpl();
		
		BigDecimal ca = commandeDAO.getCATotal();
		
		document.add(new Paragraph("Chiffre d'affaire total : " + ca + " �").setUnderline().setBold().setFontSize(16.0f));
	}
}
