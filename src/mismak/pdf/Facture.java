package mismak.pdf;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.List;

import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;

import mismak.dao.impl.CommandeDAOImpl;
import mismak.dao.impl.LignesCommandeDAOImpl;
import mismak.dao.model.CommandeDAO;
import mismak.dao.model.LignesCommandeDAO;
import mismak.model.Commande;
import mismak.model.LigneCommande;

public class Facture {
	private int commandeId;

	public Facture(int commandeId) {
		super();
		this.commandeId = commandeId;
	}

	public String genererPDF() {
		if (this.commandeId < 0)
			return null;
		
		CommandeDAO commandeDAO = new CommandeDAOImpl();
		Commande commande = commandeDAO.findByCommandeId(commandeId, true);

		PdfWriter pdfWriter;
		String fileName = commande.getId() + ".pdf";

		try {
			pdfWriter = new PdfWriter(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}

		final PdfDocument pdfDocument = new PdfDocument(pdfWriter);
		final Document document = new Document(pdfDocument, PageSize.A4);
		Paragraph titre = new Paragraph("Facture n�" + commande.getId()).setFontSize(20.0f)
				.setTextAlignment(TextAlignment.CENTER);
		document.add(titre);

		document.add(new Paragraph("Client").setUnderline());
		document.add(new Paragraph(
				commande.getClient().getNom().toUpperCase() + " " + commande.getClient().getPrenom()));
		document.add(new Paragraph(commande.getClient().getEmail()));

		document.add(genererTable(commande));
		
		if(commande.isPaye())
			document.add(new Paragraph("Facture pay�e").setFontSize(30.0f)
					.setFontColor(ColorConstants.RED)
					.setRotationAngle(Math.PI / 8)
					.setBold());
		
		document.close();
		return fileName;
	}

	private Table genererTable(Commande commande) {
		LignesCommandeDAO lignesCommandeDAO = new LignesCommandeDAOImpl();
		
		List<LigneCommande> lignesCommande = lignesCommandeDAO.findByCommandeId(commande.getId());
		
		float[] columnWidths = {10, 35, 10, 15, 15, 15};
		
		Table table = new Table(UnitValue.createPercentArray(columnWidths)).useAllAvailableWidth();
		table.addCell(new Cell().add(new Paragraph("Ref.")).setTextAlignment(TextAlignment.CENTER).setBold());
		table.addCell(new Cell().add(new Paragraph("Libell�")).setTextAlignment(TextAlignment.CENTER).setBold());
		table.addCell(new Cell().add(new Paragraph("Quantit�")).setTextAlignment(TextAlignment.CENTER).setBold());
		table.addCell(new Cell().add(new Paragraph("Prix HT")).setTextAlignment(TextAlignment.CENTER).setBold());
		table.addCell(new Cell().add(new Paragraph("Total HT")).setTextAlignment(TextAlignment.CENTER).setBold());
		table.addCell(new Cell().add(new Paragraph("Total TTC")).setTextAlignment(TextAlignment.CENTER).setBold());
		
		for(LigneCommande lc: lignesCommande) {
			table.addCell(lc.getProduit().getReference());
			table.addCell(lc.getProduit().getLibelle());
			table.addCell(""+ lc.getQuantite());
			table.addCell(lc.getProduit().getPrixHT() + " �");
			table.addCell(lc.getProduit().getPrixTTC()+ " �");
			table.addCell(lc.getProduit().getPrixTTC().multiply(new BigDecimal(lc.getQuantite())) + " �");
		}
		
		table.addCell(new Cell(1, 5).add(new Paragraph("Total HT")).setTextAlignment(TextAlignment.RIGHT).setBold());
		table.addCell(commande.getTotalHT() + " �");
		
		table.addCell(new Cell(1, 5).add(new Paragraph("TVA")).setTextAlignment(TextAlignment.RIGHT).setBold());
		table.addCell(commande.getTotalTTC().subtract(commande.getTotalHT()) + " �");
		
		table.addCell(new Cell(1, 5).add(new Paragraph("Total TTC")).setTextAlignment(TextAlignment.RIGHT).setBold());
		table.addCell(commande.getTotalTTC() + " �");
		
		return table;
	}
}
