package mismak.pdf;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;

import mismak.dao.impl.ClientDAOImpl;
import mismak.dao.impl.CommandeDAOImpl;
import mismak.dao.impl.LignesCommandeDAOImpl;
import mismak.dao.model.ClientDAO;
import mismak.dao.model.CommandeDAO;
import mismak.dao.model.LignesCommandeDAO;
import mismak.model.Client;
import mismak.model.Commande;
import mismak.model.LigneCommande;

public class InformationsClient {
	private int clientId;

	public InformationsClient(int clientId) {
		super();
		this.clientId = clientId;
	}

	public String genererPDF() {
		if (this.clientId < 0)
			return null;
		
		ClientDAO clientDAO = new ClientDAOImpl();
		Client client = clientDAO.findByClientId(this.clientId);
		
		CommandeDAO commandeDAO = new CommandeDAOImpl();
		ArrayList<Commande> commandes = commandeDAO.findByClient(client, true);
		
		LignesCommandeDAO ligneCommandeDAO = new LignesCommandeDAOImpl();
		Map<Integer, ArrayList<LigneCommande>> lignesCommande = ligneCommandeDAO.findByClientIdGroupedByCommandeId(clientId);

		PdfWriter pdfWriter;
		String fileName = "C" + client.getId() + ".pdf";

		try {
			pdfWriter = new PdfWriter(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}

		final PdfDocument pdfDocument = new PdfDocument(pdfWriter);
		final Document document = new Document(pdfDocument, PageSize.A4);
		Paragraph titre = new Paragraph("Client n�" + client.getId()).setFontSize(20.0f)
				.setTextAlignment(TextAlignment.CENTER);
		document.add(titre);
		
		Paragraph nomClient = new Paragraph(client.getNom().toUpperCase() + " " + client.getPrenom()).setFontSize(20.0f);
		document.add(nomClient);
		
		document.add(new Paragraph("Email : " + client.getEmail()));
		document.add(new Paragraph("Date de naissance : " + client.getEmail()));

		Paragraph listeCommandes = new Paragraph("Liste des commandes").setFontSize(16.0f).setBold().setUnderline();
		document.add(listeCommandes);
		
		for(Commande commande : commandes) {
			document.add(new Paragraph("Commande n�" + commande.getId() + " - " + commande.getDate()).setFontSize(14.0f));
			if(lignesCommande.get(commande.getId()) != null)
				document.add(genererTable(commande, lignesCommande.get(commande.getId())));
			document.add(new Paragraph(commande.isPaye() ? "Commande pay�e" : "Commande non pay�e").setBold().setTextAlignment(TextAlignment.CENTER));
		} 
		
		document.close();
		return fileName;
	}

	private Table genererTable(Commande commande, ArrayList<LigneCommande> lignesCommande) {
		
		float[] columnWidths = {10, 35, 10, 15, 15, 15};
		
		Table table = new Table(UnitValue.createPercentArray(columnWidths)).useAllAvailableWidth();
		table.addCell(new Cell().add(new Paragraph("Ref.")).setTextAlignment(TextAlignment.CENTER).setBold());
		table.addCell(new Cell().add(new Paragraph("Libell�")).setTextAlignment(TextAlignment.CENTER).setBold());
		table.addCell(new Cell().add(new Paragraph("Quantit�")).setTextAlignment(TextAlignment.CENTER).setBold());
		table.addCell(new Cell().add(new Paragraph("Prix HT")).setTextAlignment(TextAlignment.CENTER).setBold());
		table.addCell(new Cell().add(new Paragraph("Total HT")).setTextAlignment(TextAlignment.CENTER).setBold());
		table.addCell(new Cell().add(new Paragraph("Total TTC")).setTextAlignment(TextAlignment.CENTER).setBold());
		
		for(LigneCommande lc: lignesCommande) {
			table.addCell(lc.getProduit().getReference());
			table.addCell(lc.getProduit().getLibelle());
			table.addCell(""+ lc.getQuantite());
			table.addCell(lc.getProduit().getPrixHT() + " �");
			table.addCell(lc.getProduit().getPrixTTC()+ " �");
			table.addCell(lc.getProduit().getPrixTTC().multiply(new BigDecimal(lc.getQuantite())) + " �");
		}
		
		table.addCell(new Cell(1, 5).add(new Paragraph("Total HT")).setTextAlignment(TextAlignment.RIGHT).setBold());
		table.addCell(commande.getTotalHT() + " �");
		
		table.addCell(new Cell(1, 5).add(new Paragraph("Total TTC")).setTextAlignment(TextAlignment.RIGHT).setBold());
		table.addCell(commande.getTotalTTC() + " �");
		
		return table;
	}
}
