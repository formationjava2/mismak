package mismak.swing;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import mismak.model.CollectionProduit;
import mismak.model.Produit;
import mismak.swing.tablemodel.ProduitsTableModel;

public class ListeProduitsFrame extends JFrame implements ActionListener, ChangeListener {

	private static final long serialVersionUID = -6271614603520604299L;
	private JPanel contentPane;
	private JTable table;
	private ProduitsTableModel produitsTableModel;

	private CollectionProduit collectionProduits = new CollectionProduit();
	private JButton btnAjouter;
	private JButton btnEditer;
	private JButton btnSupprimer;
	private JCheckBox chckbxArchive;
	private JButton btnDesarchiver;

	/**
	 * Create the frame.
	 */
	public ListeProduitsFrame() {

		setTitle("Gestion des produits");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		produitsTableModel = new ProduitsTableModel(this.collectionProduits);
		table = new JTable(produitsTableModel);

		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setAutoCreateColumnsFromModel(true);
		table.setAutoCreateRowSorter(true);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		contentPane.add(scrollPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);

		btnAjouter = new JButton("Ajouter");
		btnAjouter.addActionListener(this);
		panel.add(btnAjouter);

		btnEditer = new JButton("Editer");
		btnEditer.addActionListener(this);
		panel.add(btnEditer);

		btnSupprimer = new JButton("Supprimer");
		btnSupprimer.addActionListener(this);
		panel.add(btnSupprimer);
		
		btnDesarchiver = new JButton("D\u00E9sarchiver");
		btnDesarchiver.setVisible(false);
		btnDesarchiver.addActionListener(this);
		panel.add(btnDesarchiver);

		chckbxArchive = new JCheckBox("Archive");
		chckbxArchive.addChangeListener(this);
		contentPane.add(chckbxArchive, BorderLayout.NORTH);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.getBtnAjouter())
			this.ajouterProduitAction();
		else if (e.getSource() == this.getBtnEditer())
			this.modifierProduitAction();
		else if (e.getSource() == this.getBtnSupprimer())
			this.supprimerProduitAction();
		else if(e.getSource() == this.getBtnDesarchiver())
			this.desarchiverProduitAction();

	}

	protected JButton getBtnAjouter() {
		return btnAjouter;
	}

	private void ajouterProduitAction() {
		this.ajouterProduitAction(null);
	}

	private void ajouterProduitAction(FormulaireProduit formulaireProduit) {

		if (formulaireProduit == null)
			formulaireProduit = new FormulaireProduit();

		int resultat = JOptionPane.showConfirmDialog(this, formulaireProduit, "Ajout d'un produit",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (resultat == JOptionPane.OK_OPTION) {
			String reference = formulaireProduit.getChampReference().getText().trim();
			String libelle = formulaireProduit.getChampLibelle().getText().trim();
			String prix = formulaireProduit.getChampPrix().getText().trim();

			// Champs corrects ?
			if (reference.equals("")) {
				JOptionPane.showMessageDialog(this, "La r�f�rence est obligatoire", "Erreur",
						JOptionPane.ERROR_MESSAGE);
				this.ajouterProduitAction(formulaireProduit);
				return;
			} else if (libelle.equals("")) {
				JOptionPane.showMessageDialog(this, "Le libell� est obligatoire", "Erreur", JOptionPane.ERROR_MESSAGE);
				this.ajouterProduitAction(formulaireProduit);
				return;
			} else if (prix.equals("")) {
				JOptionPane.showMessageDialog(this, "Le prix est obligatoire", "Erreur", JOptionPane.ERROR_MESSAGE);
				this.ajouterProduitAction(formulaireProduit);
				return;
			} else {
				Produit nouveauProduit = new Produit(reference, libelle, new BigDecimal(prix));

				boolean resultatAjout = this.collectionProduits.ajouter(nouveauProduit);
				if (!resultatAjout) {
					JOptionPane.showMessageDialog(this, "Erreur lors de l'ajout du produit.", "Erreur",
							JOptionPane.ERROR_MESSAGE);
				}

				this.produitsTableModel.fireTableDataChanged();
			}
		}
	}

	private void modifierProduitAction() {
		this.modifierProduitAction(null, null);
	}

	private void modifierProduitAction(FormulaireProduit formulaireProduit, Produit produit) {
		if (formulaireProduit == null) {
			if (this.table.getSelectedRow() == -1)
				return;

			produit = this.collectionProduits.getListeProduits()
					.get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));
			formulaireProduit = new FormulaireProduit(produit.getReference(), produit.getLibelle(),
					produit.getPrixHT().toString());
		}

		int resultat = JOptionPane.showConfirmDialog(this, formulaireProduit, "Modification d'un produit",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (resultat == JOptionPane.OK_OPTION) {
			String reference = formulaireProduit.getChampReference().getText().trim();
			String libelle = formulaireProduit.getChampLibelle().getText().trim();
			String prix = formulaireProduit.getChampPrix().getText().trim();

			// Champs corrects ?
			if (reference.equals("")) {
				JOptionPane.showMessageDialog(this, "La r�f�rence est obligatoire", "Erreur",
						JOptionPane.ERROR_MESSAGE);
				this.modifierProduitAction(formulaireProduit, produit);
				return;
			} else if (libelle.equals("")) {
				JOptionPane.showMessageDialog(this, "Le libell� est obligatoire", "Erreur", JOptionPane.ERROR_MESSAGE);
				this.modifierProduitAction(formulaireProduit, produit);
				return;
			} else if (prix.equals("")) {
				JOptionPane.showMessageDialog(this, "Le prix est obligatoire", "Erreur", JOptionPane.ERROR_MESSAGE);
				this.modifierProduitAction(formulaireProduit, produit);
				return;
			} else {
				produit.setReference(reference);
				produit.setLibelle(libelle);
				produit.setPrixHT(new BigDecimal(prix));

				boolean resultatModification = this.collectionProduits.modifier(produit);
				if (!resultatModification) {
					JOptionPane.showMessageDialog(this, "Erreur lors de la modification du produit.", "Erreur",
							JOptionPane.ERROR_MESSAGE);
				}

				this.produitsTableModel.fireTableDataChanged();
			}
		}

	}

	private void supprimerProduitAction() {
		if (this.table.getSelectedRow() == -1)
			return;

		Produit produit = this.collectionProduits.getListeProduits()
				.get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));

		boolean resultatSuppression = this.collectionProduits.supprimer(produit);
		if (!resultatSuppression) {
			JOptionPane.showMessageDialog(this, "Erreur lors de la suppression du produit.", "Erreur",
					JOptionPane.ERROR_MESSAGE);
		}

		this.produitsTableModel.fireTableDataChanged();
	}

	private void desarchiverProduitAction() {
		if(this.table.getSelectedRow() == -1)
			return;
		
		Produit produit = this.collectionProduits.getListeProduits()
				.get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));		
		
		boolean resultatSuppression = this.collectionProduits.setArchive(produit, false);
		if (!resultatSuppression) {
			JOptionPane.showMessageDialog(this, "Erreur lors du d�sarchivage du produit.", "Erreur",
					JOptionPane.ERROR_MESSAGE);
		}
		
		this.produitsTableModel.fireTableDataChanged();
		
	}
	
	public JButton getBtnEditer() {
		return btnEditer;
	}

	public JButton getBtnSupprimer() {
		return btnSupprimer;
	}

	protected JCheckBox getChckbxArchive() {
		return chckbxArchive;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() == this.getChckbxArchive()) {
			this.collectionProduits.setSelectedArchive(this.getChckbxArchive().isSelected());
			this.getBtnDesarchiver().setVisible(this.getChckbxArchive().isSelected());
			this.produitsTableModel.fireTableStructureChanged();
		}

	}
	protected JButton getBtnDesarchiver() {
		return btnDesarchiver;
	}
}
