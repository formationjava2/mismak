package mismak.swing;

import java.awt.GridLayout;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FormulaireClient extends JPanel {

	private static final long serialVersionUID = 181454947375235937L;

	private JTextField champNom;	
	private JTextField champPrenom;
	private JFormattedTextField champDateNaissance;
	private JTextField champMail;
	
	public FormulaireClient() {
		super(new GridLayout(0,1));
				
		this.add(new JLabel("Nom:"));
		this.champNom = new JTextField();
		this.add(this.champNom);
		
		this.add(new JLabel("Pr\u00E9nom:"));
		this.champPrenom = new JTextField();
		this.add(this.champPrenom);
		
		this.add(new JLabel("Date de naissance:"));
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		this.champDateNaissance = new JFormattedTextField(format);
		this.add(this.champDateNaissance);
		
		this.add(new JLabel("Mail:"));
		
		this.champMail = new JTextField();			
		this.add(this.champMail);		
		
	}
	
	public FormulaireClient(String nom, String prenom, String dateNaissance, String mail) {
		this();
		this.champNom.setText(nom);
		this.champDateNaissance.setText(dateNaissance);
		this.champPrenom.setText(prenom);
		this.champMail.setText(mail);		
	}
	
	public JTextField getChampNom() {
		return champNom;
	}

	public JTextField getChampPrenom() {
		return champPrenom;
	}

	public JTextField getChampDateNaissance() {
		return champDateNaissance;
	}

	public JTextField getChampMail() {
		return champMail;
	}
}
