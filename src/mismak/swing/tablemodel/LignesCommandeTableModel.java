package mismak.swing.tablemodel;

import java.math.BigDecimal;

import javax.swing.table.DefaultTableModel;

import mismak.model.CollectionLignesCommande;

public class LignesCommandeTableModel extends DefaultTableModel {

	private static final long serialVersionUID = -5506218406065675891L;
	
	private String[] entetes = { "R�f�rence", "Libell�", "Prix HT", "Quantit�", "Total HT" };
	private CollectionLignesCommande collectionLignesCommandes;
	
	public LignesCommandeTableModel(CollectionLignesCommande collectionLignesCommandes) {
		super();
		this.collectionLignesCommandes = collectionLignesCommandes;
	}
	
	@Override
	public int getColumnCount() {
		return  this.entetes.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	}
	
	@Override
	public int getRowCount() {
		return this.collectionLignesCommandes == null ? 0 : this.collectionLignesCommandes.getListeLignesCommande().size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
		case 0:
			return this.collectionLignesCommandes.getListeLignesCommande().get(rowIndex).getProduit().getReference();
		case 1:
			return this.collectionLignesCommandes.getListeLignesCommande().get(rowIndex).getProduit().getLibelle();
		case 2:			
			return this.collectionLignesCommandes.getListeLignesCommande().get(rowIndex).getProduit().getPrixHT();
		case 3:
			return this.collectionLignesCommandes.getListeLignesCommande().get(rowIndex).getQuantite();
		case 4:
			return this.collectionLignesCommandes.getListeLignesCommande().get(rowIndex).getProduit().getPrixHT().multiply(
						new BigDecimal(this.collectionLignesCommandes.getListeLignesCommande().get(rowIndex).getQuantite())
					);
		default:
			throw new IllegalArgumentException();
		}
	}	
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch(columnIndex) {		
		case 0 :
		case 1 :
			return String.class;
		case 2:
		case 4:
			return BigDecimal.class;
		case 3:
			return Integer.class;
		default:
			return Object.class;
		}
	}
	
	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
}
