package mismak.swing.tablemodel;

import java.util.Date;

import javax.swing.table.DefaultTableModel;

import mismak.model.CollectionClient;

public class ClientsTableModel extends DefaultTableModel {

	private static final long serialVersionUID = -5506218406065675891L;
	
	private String[] entetes = { "Archive", "Id", "Nom", "Pr�nom", "Date de naissance", "Mail" };
	private CollectionClient collectionClients;
	
	public ClientsTableModel(CollectionClient collectionClients) {
		super();
		this.collectionClients = collectionClients;
	}
	
	@Override
	public int getColumnCount() {
		return  this.collectionClients.isSelectedArchive() ?  this.entetes.length : this.entetes.length - 1;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return this.collectionClients.isSelectedArchive() ? entetes[columnIndex] : entetes[columnIndex + 1];
	}
	
	@Override
	public int getRowCount() {
		return this.collectionClients == null ? 0 : this.collectionClients.getListeClients().size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return this.collectionClients.isSelectedArchive() ? this.getValueAtArchive(rowIndex, columnIndex) : this.getValueAtNoArchive(rowIndex, columnIndex);
	}
	
	public Object getValueAtNoArchive(int rowIndex, int columnIndex) {
		switch(columnIndex) {
		case 0:
			return this.collectionClients.getListeClients().get(rowIndex).getId();
		case 1:
			return this.collectionClients.getListeClients().get(rowIndex).getNom();
		case 2:			
			return this.collectionClients.getListeClients().get(rowIndex).getPrenom();
		case 3:
			return this.collectionClients.getListeClients().get(rowIndex).getDateNaissance();
		case 4:
			return this.collectionClients.getListeClients().get(rowIndex).getEmail();
		default:
			throw new IllegalArgumentException();
		}
	}
	
	public Object getValueAtArchive(int rowIndex, int columnIndex) {
		switch(columnIndex) {
		case 0:
			return this.collectionClients.getListeClients().get(rowIndex).isArchive() ? "X" : "";
		case 1:
			return this.collectionClients.getListeClients().get(rowIndex).getId();
		case 2:
			return this.collectionClients.getListeClients().get(rowIndex).getNom();
		case 3:			
			return this.collectionClients.getListeClients().get(rowIndex).getPrenom();
		case 4:
			return this.collectionClients.getListeClients().get(rowIndex).getDateNaissance();
		case 5:
			return this.collectionClients.getListeClients().get(rowIndex).getEmail();
		default:
			throw new IllegalArgumentException();
		}
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return this.collectionClients.isSelectedArchive() ? this.getColumnClassArchive(columnIndex) : this.getColumnClassNoArchive(columnIndex);
	}
	
	public Class<?> getColumnClassNoArchive(int columnIndex) {
	
		switch(columnIndex) {		
		case 0 :
			return Integer.class;
		case 1:
		case 2:
		case 4:
			return String.class;
		case 3:
			return Date.class;
		default:
			return Object.class;
		}
	}
	
	public Class<?> getColumnClassArchive(int columnIndex) {
		
		switch(columnIndex) {		
		case 1 :
			return Integer.class;
		case 0:
		case 2:
		case 5:
			return String.class;
		case 4:
			return Date.class;
		default:
			return Object.class;
		}
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

}
