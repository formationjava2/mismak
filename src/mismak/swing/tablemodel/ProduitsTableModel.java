package mismak.swing.tablemodel;

import java.math.BigDecimal;

import javax.swing.table.DefaultTableModel;

import mismak.model.CollectionProduit;

public class ProduitsTableModel extends DefaultTableModel {

	private static final long serialVersionUID = -5506218406065675891L;
	
	private String[] entetes = { "Archive", "Id", "R�f�rence", "Libell�", "Prix" };
	private CollectionProduit collectionProduits;
	
	public ProduitsTableModel(CollectionProduit collectionProduits) {
		super();
		this.collectionProduits = collectionProduits;
	}
	
	@Override
	public int getColumnCount() {
		return  this.collectionProduits.isSelectedArchive() ?  this.entetes.length : this.entetes.length - 1;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return this.collectionProduits.isSelectedArchive() ? entetes[columnIndex] : entetes[columnIndex + 1];
	}
	
	@Override
	public int getRowCount() {
		return this.collectionProduits == null ? 0 : this.collectionProduits.getListeProduits().size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return this.collectionProduits.isSelectedArchive() ? this.getValueAtArchive(rowIndex, columnIndex) : this.getValueAtNoArchive(rowIndex, columnIndex);
	}
	
	public Object getValueAtNoArchive(int rowIndex, int columnIndex) {
		switch(columnIndex) {
		case 0:
			return this.collectionProduits.getListeProduits().get(rowIndex).getId();
		case 1:
			return this.collectionProduits.getListeProduits().get(rowIndex).getReference();
		case 2:			
			return this.collectionProduits.getListeProduits().get(rowIndex).getLibelle();
		case 3:
			return this.collectionProduits.getListeProduits().get(rowIndex).getPrixHT();
		default:
			throw new IllegalArgumentException();
		}
	}
	
	public Object getValueAtArchive(int rowIndex, int columnIndex) {
		switch(columnIndex) {
		case 0:
			return this.collectionProduits.getListeProduits().get(rowIndex).isArchive() ? "X" : "";
		case 1:
			return this.collectionProduits.getListeProduits().get(rowIndex).getId();
		case 2:
			return this.collectionProduits.getListeProduits().get(rowIndex).getReference();
		case 3:			
			return this.collectionProduits.getListeProduits().get(rowIndex).getLibelle();
		case 4:
			return this.collectionProduits.getListeProduits().get(rowIndex).getPrixHT();
		default:
			throw new IllegalArgumentException();
		}
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return this.collectionProduits.isSelectedArchive() ? this.getColumnClassArchive(columnIndex) : this.getColumnClassNoArchive(columnIndex);
	}
	
	public Class<?> getColumnClassNoArchive(int columnIndex) {
	
		switch(columnIndex) {		
		case 0 :
			return Integer.class;
		case 1:
		case 2:
			return String.class;
		case 3:
			return BigDecimal.class;
		default:
			return Object.class;
		}
	}
	
	public Class<?> getColumnClassArchive(int columnIndex) {
		
		switch(columnIndex) {		
		case 1 :
			return Integer.class;
		case 0:
		case 2:
		case 3:
			return String.class;
		case 4:
			return BigDecimal.class;
		default:
			return Object.class;
		}
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
}
