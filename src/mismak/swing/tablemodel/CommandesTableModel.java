package mismak.swing.tablemodel;

import java.math.BigDecimal;
import java.util.Date;

import javax.swing.table.DefaultTableModel;

import com.mysql.cj.xdevapi.Client;

import mismak.model.CollectionCommande;

public class CommandesTableModel extends DefaultTableModel {

	private static final long serialVersionUID = -5506218406065675891L;
	
	private String[] entetes = { "Num�ro", "Date", "Client", "Total HT", "Total TTC", "Pay�" };
	private CollectionCommande collectionCommandes;
	
	public CommandesTableModel(CollectionCommande collectionCommandes) {
		super();
		this.collectionCommandes = collectionCommandes;
	}
	
	@Override
	public int getColumnCount() {
		return  this.entetes.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	}
	
	@Override
	public int getRowCount() {
		return this.collectionCommandes == null ? 0 : this.collectionCommandes.getListeCommandes().size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
		case 0:
			return this.collectionCommandes.getListeCommandes().get(rowIndex).getId();
		case 1:
			return this.collectionCommandes.getListeCommandes().get(rowIndex).getDate();
		case 2:			
			return this.collectionCommandes.getListeCommandes().get(rowIndex).getClient();
		case 3:
			return this.collectionCommandes.getListeCommandes().get(rowIndex).getTotalHT();
		case 4:
			return this.collectionCommandes.getListeCommandes().get(rowIndex).getTotalTTC();
		case 5:
			return this.collectionCommandes.getListeCommandes().get(rowIndex).isPaye() ? "X" : "";
		default:
			throw new IllegalArgumentException();
		}
	}	
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch(columnIndex) {		
		case 0 :
			return Integer.class;
		case 1:
			return Date.class;
		case 2:
			return Client.class;
		case 3:
		case 4:
			return BigDecimal.class;
		case 5:
			return String.class;
		default:
			return Object.class;
		}
	}
	
	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
}
