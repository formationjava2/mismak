package mismak.swing;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import mismak.model.Client;
import mismak.model.CollectionClient;
import mismak.pdf.InformationsClient;
import mismak.swing.tablemodel.ClientsTableModel;

public class ListeClientsFrame extends JFrame implements ActionListener, ChangeListener {

	private static final long serialVersionUID = -6271614603520604299L;
	private JPanel contentPane;
	private JTable table;
	private ClientsTableModel clientsTableModel;

	private CollectionClient collectionClients = new CollectionClient();
	private JButton btnAjouter;
	private JButton btnEditer;
	private JButton btnSupprimer;
	private JCheckBox chckbxArchive;
	private JButton btnDesarchiver;
	private JButton btnCommandes;
	private JButton btnExporter;

	/**
	 * Create the frame.
	 */
	public ListeClientsFrame() {
		
		setTitle("Gestion des clients");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 581, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		clientsTableModel = new ClientsTableModel(this.collectionClients);
		table = new JTable(clientsTableModel);
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setAutoCreateColumnsFromModel(true);
		table.setAutoCreateRowSorter(true);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		contentPane.add(scrollPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);

		btnAjouter = new JButton("Ajouter");
		btnAjouter.addActionListener(this);
		panel.add(btnAjouter);

		btnEditer = new JButton("Editer");
		btnEditer.addActionListener(this);
		panel.add(btnEditer);

		btnSupprimer = new JButton("Supprimer");
		btnSupprimer.addActionListener(this);
		panel.add(btnSupprimer);
		
		btnDesarchiver = new JButton("D\u00E9sarchiver");
		btnDesarchiver.setVisible(false);
		btnDesarchiver.addActionListener(this);
		panel.add(btnDesarchiver);
		
		btnCommandes = new JButton("Commandes");
		btnCommandes.addActionListener(this);
		panel.add(btnCommandes);
		
		btnExporter = new JButton("Exporter");
		btnExporter.addActionListener(this);
		panel.add(btnExporter);
		
		chckbxArchive = new JCheckBox("Archive");
		chckbxArchive.addChangeListener(this);
		contentPane.add(chckbxArchive, BorderLayout.NORTH);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.getBtnAjouter())
			this.ajouterClientAction();
		else if(e.getSource() == this.getBtnEditer())
			this.modifierClientAction();
		else if(e.getSource() == this.getBtnSupprimer())
			this.supprimerClientAction();
		else if(e.getSource() == this.getBtnDesarchiver())
			this.desarchiverClientAction();
		else if(e.getSource() == this.getBtnCommandes())
			this.voirCommandesClientAction();
		else if(e.getSource() == this.getBtnExporter())
			this.exporterPDFAction();
	}

	private void voirCommandesClientAction() {
		if(this.table.getSelectedRow() == -1)
			return;
		
		Client client = this.collectionClients.getListeClients()
				.get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));
		
		ListeCommandesFrame commandesFrame = new ListeCommandesFrame(client);
		commandesFrame.setTitle("Commandes de " + client);
		commandesFrame.setVisible(true);
	}

	protected JButton getBtnAjouter() {
		return btnAjouter;
	}

	private void ajouterClientAction() {
		this.ajouterClientAction(null);
	}

	private void ajouterClientAction(FormulaireClient formulaireClient) {

		if (formulaireClient == null)
			formulaireClient = new FormulaireClient();

		int resultat = JOptionPane.showConfirmDialog(this, formulaireClient, "Ajout d'un client",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (resultat == JOptionPane.OK_OPTION) {
			String nom = formulaireClient.getChampNom().getText().trim();
			String prenom = formulaireClient.getChampPrenom().getText().trim();
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

			// Champs corrects ?
			if (nom.equals("")) {
				JOptionPane.showMessageDialog(this, "Le nom est obligatoire", "Erreur", JOptionPane.ERROR_MESSAGE);
				this.ajouterClientAction(formulaireClient);
				return;
			} else if (prenom.equals("")) {
				JOptionPane.showMessageDialog(this, "Le pr�nom est obligatoire", "Erreur", JOptionPane.ERROR_MESSAGE);
				this.ajouterClientAction(formulaireClient);
				return;
			} else if (formulaireClient.getChampDateNaissance().getText().trim().equals("")) {
				JOptionPane.showMessageDialog(this, "La date de naissance est obligatoire", "Erreur",
						JOptionPane.ERROR_MESSAGE);
				this.ajouterClientAction(formulaireClient);
				return;
			} else {
				Date dateNaissance;
				try {
					dateNaissance = format.parse(formulaireClient.getChampDateNaissance().getText().trim());
					String mail = formulaireClient.getChampMail().getText().trim();

					Client nouveauClient = new Client(nom, prenom, dateNaissance, mail);

					boolean resultatAjout = this.collectionClients.ajouter(nouveauClient);
					if (!resultatAjout) {
						JOptionPane.showMessageDialog(this, "Erreur lors de l'ajout du client.", "Erreur",
								JOptionPane.ERROR_MESSAGE);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}

				this.clientsTableModel.fireTableDataChanged();
			}
		}
	}
	
	private void modifierClientAction() {
		this.modifierClientAction(null, null);		
	}

	private void modifierClientAction(FormulaireClient formulaireClient, Client client) {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		if (formulaireClient == null) {
			if(this.table.getSelectedRow() == -1)
				return;
			
			client = this.collectionClients.getListeClients()
					.get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));			
			formulaireClient = new FormulaireClient(client.getNom(), client.getPrenom(), format.format(client.getDateNaissance()), client.getEmail());
		}
		
		int resultat = JOptionPane.showConfirmDialog(this, formulaireClient, "Modification d'un client",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (resultat == JOptionPane.OK_OPTION) {
			String nom = formulaireClient.getChampNom().getText().trim();
			String prenom = formulaireClient.getChampPrenom().getText().trim();

			// Champs corrects ?
			if (nom.equals("")) {
				JOptionPane.showMessageDialog(this, "Le nom est obligatoire", "Erreur", JOptionPane.ERROR_MESSAGE);
				this.modifierClientAction(formulaireClient, client);
				return;
			} else if (prenom.equals("")) {
				JOptionPane.showMessageDialog(this, "Le pr�nom est obligatoire", "Erreur", JOptionPane.ERROR_MESSAGE);
				this.modifierClientAction(formulaireClient, client);
				return;
			} else if (formulaireClient.getChampDateNaissance().getText().trim().equals("")) {
				JOptionPane.showMessageDialog(this, "La date de naissance est obligatoire", "Erreur",
						JOptionPane.ERROR_MESSAGE);
				this.modifierClientAction(formulaireClient, client);
				return;
			} else {
				Date dateNaissance;
				try {
					dateNaissance = format.parse(formulaireClient.getChampDateNaissance().getText().trim());
					String mail = formulaireClient.getChampMail().getText().trim();

					client.setNom(nom);
					client.setPrenom(prenom);
					client.setDateNaissance(dateNaissance);
					client.setEmail(mail);
										
					boolean resultatModification = this.collectionClients.modifier(client);
					if (!resultatModification) {
						JOptionPane.showMessageDialog(this, "Erreur lors de la modification du client.", "Erreur",
								JOptionPane.ERROR_MESSAGE);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}

				this.clientsTableModel.fireTableDataChanged();
			}
		}
		
	}
	
	private void supprimerClientAction() {
		if(this.table.getSelectedRow() == -1)
			return;
		
		Client client = this.collectionClients.getListeClients()
					.get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));			
		
		boolean resultatSuppression = this.collectionClients.supprimer(client);
		if (!resultatSuppression) {
			JOptionPane.showMessageDialog(this, "Erreur lors de la suppression du client.", "Erreur",
					JOptionPane.ERROR_MESSAGE);
		}
		
		this.clientsTableModel.fireTableDataChanged();
		
	}
	
	private void desarchiverClientAction() {
		if(this.table.getSelectedRow() == -1)
			return;
		
		Client client = this.collectionClients.getListeClients()
					.get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));			
		
		boolean resultatSuppression = this.collectionClients.setArchive(client, false);
		if (!resultatSuppression) {
			JOptionPane.showMessageDialog(this, "Erreur lors du d�sarchivage du client.", "Erreur",
					JOptionPane.ERROR_MESSAGE);
		}
		
		this.clientsTableModel.fireTableDataChanged();
		
	}
	
	private void exporterPDFAction() {

		if (this.table.getSelectedRow() == -1)
			return;
		
		Client client = this.collectionClients.getListeClients()
				.get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));

		InformationsClient informationsClient = new InformationsClient(client.getId());
		
		String fichier = informationsClient.genererPDF();
		if(fichier != null) {
			JOptionPane.showMessageDialog(this, "Le fichier " + fichier + " �t� g�n�r�.", "Client",
					JOptionPane.INFORMATION_MESSAGE);
		}
		else {
			JOptionPane.showMessageDialog(this, "Erreur lors de la cr�ation du fichier client.", "Erreur",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public JButton getBtnEditer() {
		return btnEditer;
	}
	public JButton getBtnSupprimer() {
		return btnSupprimer;
	}
	protected JCheckBox getChckbxArchive() {
		return chckbxArchive;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if(e.getSource() == this.getChckbxArchive()) {
			this.collectionClients.setSelectedArchive(this.getChckbxArchive().isSelected());
			this.clientsTableModel.fireTableStructureChanged();
			this.btnDesarchiver.setVisible(this.getChckbxArchive().isSelected());
		}
		
	}
	public JButton getBtnDesarchiver() {
		return btnDesarchiver;
	}
	protected JButton getBtnCommandes() {
		return btnCommandes;
	}
	protected JButton getBtnExporter() {
		return btnExporter;
	}
}
