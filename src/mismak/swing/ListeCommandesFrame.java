package mismak.swing;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import mismak.model.Client;
import mismak.model.CollectionCommande;
import mismak.model.CollectionLignesCommande;
import mismak.model.Commande;
import mismak.pdf.Facture;
import mismak.swing.tablemodel.CommandesTableModel;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class ListeCommandesFrame extends JFrame implements ActionListener {

	private static final long serialVersionUID = -6271614603520604299L;
	private JPanel contentPane;
	private JTable table;
	private CommandesTableModel commandesTableModel;
	private CommandesWindowListener windowListener;

	private CollectionCommande collectionCommandes;
	private JButton btnAjouter;
	private JButton btnEditer;
	private JButton btnSupprimer;
	private JButton btnPaye;
	private JButton btnLignesCommandes;
	private JPanel recherchePanel;
	private JLabel lblRecherche;
	private JTextField rechercheTextField;
	private JButton btnRecherche;
	private JButton btnFacture;

	/**
	 * Create the frame.
	 */
	private ListeCommandesFrame() {

		setTitle("Gestion des commandes");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 850, 495);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		recherchePanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) recherchePanel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(recherchePanel, BorderLayout.NORTH);
		
		lblRecherche = new JLabel("Rechercher un num\u00E9ro de commande :");
		recherchePanel.add(lblRecherche);
		
		rechercheTextField = new JTextField();
		recherchePanel.add(rechercheTextField);
		rechercheTextField.setColumns(10);
		
		btnRecherche = new JButton("Rechercher");
		btnRecherche.addActionListener(this);
		recherchePanel.add(btnRecherche);

		table = new JTable();

		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setAutoCreateColumnsFromModel(true);
		table.setAutoCreateRowSorter(true);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		contentPane.add(scrollPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);

		btnAjouter = new JButton("Ajouter");
		btnAjouter.addActionListener(this);
		panel.add(btnAjouter);

		btnEditer = new JButton("Modifier");
		btnEditer.addActionListener(this);

		btnSupprimer = new JButton("Supprimer");
		btnSupprimer.addActionListener(this);
		panel.add(btnSupprimer);
		panel.add(btnEditer);

		btnPaye = new JButton("Marquer pay\u00E9");
		btnPaye.addActionListener(this);

		btnLignesCommandes = new JButton("Lignes de commande");
		btnLignesCommandes.addActionListener(this);
		panel.add(btnLignesCommandes);
		panel.add(btnPaye);
		
		btnFacture = new JButton("Editer la facture");
		btnFacture.addActionListener(this);
		panel.add(btnFacture);
		
	}
	
	public ListeCommandesFrame(Client client) {
		this();
		
		this.collectionCommandes = (client == null ? new CollectionCommande() : new CollectionCommande(client));
		this.commandesTableModel = new CommandesTableModel(this.collectionCommandes);
		this.table.setModel(commandesTableModel);
		
		this.windowListener = new CommandesWindowListener(commandesTableModel, collectionCommandes);

	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.getBtnAjouter())
			this.ajouterCommandeAction();
		else if (e.getSource() == this.getBtnEditer())
			this.modifierCommandeAction();
		else if (e.getSource() == this.getBtnSupprimer())
			this.supprimerCommandeAction();
		else if (e.getSource() == this.getBtnPaye())
			this.payerCommandeAction();
		else if (e.getSource() == this.getBtnLignesCommandes())
			this.editerLignesCommandeAction();
		else if (e.getSource() == this.getBtnRecherche())
			this.rechercherCommandeAction();
		else if (e.getSource() == this.getBtnFacture())
			this.editerFactureAction();
			
	}

	private void payerCommandeAction() {
		if (this.table.getSelectedRow() == -1)
			return;

		Commande commande = this.collectionCommandes.getListeCommandes()
				.get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));

		boolean resultatModification = this.collectionCommandes.setPaye(commande);
		if (!resultatModification) {
			JOptionPane.showMessageDialog(this, "Erreur lors de la modification de la commande.", "Erreur",
					JOptionPane.ERROR_MESSAGE);
		}

		this.commandesTableModel.fireTableDataChanged();
	}

	protected JButton getBtnAjouter() {
		return btnAjouter;
	}

	private void ajouterCommandeAction() {
		this.ajouterCommandeAction(null);
	}

	private void ajouterCommandeAction(FormulaireCommande formulaireCommande) {

		if (formulaireCommande == null)
			formulaireCommande = new FormulaireCommande();

		int resultat = JOptionPane.showConfirmDialog(this, formulaireCommande, "Ajout d'une commande",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

		if (resultat == JOptionPane.OK_OPTION) {
			Client client = (Client) formulaireCommande.getComboBoxClient().getSelectedItem();
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

			// Champs corrects ?
			if (formulaireCommande.getChampDate().getText().trim().equals("")) {
				JOptionPane.showMessageDialog(this, "La date est obligatoire", "Erreur", JOptionPane.ERROR_MESSAGE);
				this.ajouterCommandeAction(formulaireCommande);
				return;
			} else {
				Date dateCommande;
				try {
					dateCommande = format.parse(formulaireCommande.getChampDate().getText().trim());

					Commande nouvelleCommande = new Commande(dateCommande, client, false);

					boolean resultatAjout = this.collectionCommandes.ajouter(nouvelleCommande);
					if (!resultatAjout) {
						JOptionPane.showMessageDialog(this, "Erreur lors de l'ajout de la commande.", "Erreur",
								JOptionPane.ERROR_MESSAGE);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}

				this.commandesTableModel.fireTableDataChanged();
			}
		}
	}

	private void modifierCommandeAction() {
		this.modifierCommandeAction(null, null);
	}

	private void modifierCommandeAction(FormulaireCommande formulaireCommande, Commande commande) {
		if (formulaireCommande == null) {
			if (this.table.getSelectedRow() == -1)
				return;

			commande = this.collectionCommandes.getListeCommandes()
					.get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));

			if (commande.isPaye()) {
				JOptionPane.showMessageDialog(this, "Il n'est pas possible de modifier une commande pay�e.",
						"Attention", JOptionPane.WARNING_MESSAGE);
				return;
			}

			DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			formulaireCommande = new FormulaireCommande(commande.getClient(), format.format(commande.getDate()));
		}

		int resultat = JOptionPane.showConfirmDialog(this, formulaireCommande, "Modification d'une commande",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (resultat == JOptionPane.OK_OPTION) {
			Client client = (Client) formulaireCommande.getComboBoxClient().getSelectedItem();
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

			// Champs corrects ?
			if (formulaireCommande.getChampDate().getText().trim().equals("")) {
				JOptionPane.showMessageDialog(this, "La date est obligatoire", "Erreur", JOptionPane.ERROR_MESSAGE);
				this.modifierCommandeAction(formulaireCommande, commande);
				return;
			} else {
				try {
					commande.setClient(client);
					commande.setDate(format.parse(formulaireCommande.getChampDate().getText().trim()));

					boolean resultatModification = this.collectionCommandes.modifier(commande);
					if (!resultatModification) {
						JOptionPane.showMessageDialog(this, "Erreur lors de la modification de la commande.", "Erreur",
								JOptionPane.ERROR_MESSAGE);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}

				this.commandesTableModel.fireTableDataChanged();

			}
		}
	}

	private void editerLignesCommandeAction() {
		if (this.table.getSelectedRow() == -1)
			return;

		Commande commande = this.collectionCommandes.getListeCommandes()
				.get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));
		
		ListeLignesCommandeFrame listeLignesCommandeFrame = new ListeLignesCommandeFrame(new CollectionLignesCommande(commande), commande.isPaye(), this.windowListener);
		listeLignesCommandeFrame.setTitle("Edition de la commande " + commande.getId());
		listeLignesCommandeFrame.setVisible(true);
	}

	private void supprimerCommandeAction() {
		if (this.table.getSelectedRow() == -1)
			return;

		Commande commande = this.collectionCommandes.getListeCommandes()
				.get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));

		if (commande.isPaye()) {
			JOptionPane.showMessageDialog(this, "Il n'est pas possible de supprimer une commande pay�e.", "Attention",
					JOptionPane.WARNING_MESSAGE);
			return;
		}

		boolean resultatSuppression = this.collectionCommandes.supprimer(commande);
		if (!resultatSuppression) {
			JOptionPane.showMessageDialog(this, "Erreur lors de la suppression de la commande.", "Erreur",
					JOptionPane.ERROR_MESSAGE);
		}

		this.commandesTableModel.fireTableDataChanged();
	}
	
	private void rechercherCommandeAction() {
		if(this.rechercheTextField.getText().trim().equals(""))
			this.collectionCommandes.rechercher(null);
		else {
			try {
				this.collectionCommandes.rechercher(Integer.parseInt(this.rechercheTextField.getText()));
			}
			catch(NumberFormatException e ) {
				this.collectionCommandes.rechercher(null);
			}
		}
		
		this.commandesTableModel.fireTableDataChanged();
	}
	
	private void editerFactureAction() {

		if (this.table.getSelectedRow() == -1)
			return;
		
		Commande commande = this.collectionCommandes.getListeCommandes()
				.get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));

		Facture facture = new Facture(commande.getId());
		
		String fichier = facture.genererPDF();
		if(fichier != null) {
			JOptionPane.showMessageDialog(this, "Le fichier " + fichier + " �t� g�n�r�.", "Facture",
					JOptionPane.INFORMATION_MESSAGE);
		}
		else {
			JOptionPane.showMessageDialog(this, "Erreur lors de la cr�ation du fichier de facture.", "Erreur",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public JButton getBtnEditer() {
		return btnEditer;
	}

	public JButton getBtnSupprimer() {
		return btnSupprimer;
	}

	protected JButton getBtnPaye() {
		return btnPaye;
	}

	protected JButton getBtnLignesCommandes() {
		return btnLignesCommandes;
	}
	protected JButton getBtnRecherche() {
		return btnRecherche;
	}
	protected JButton getBtnFacture() {
		return btnFacture;
	}
}
