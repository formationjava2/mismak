package mismak.swing;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import mismak.model.CollectionLignesCommande;
import mismak.model.LigneCommande;
import mismak.model.Produit;
import mismak.swing.tablemodel.LignesCommandeTableModel;

public class ListeLignesCommandeFrame extends JFrame implements ActionListener {

	private static final long serialVersionUID = -6271614603520604299L;
	private JPanel contentPane;
	private JTable table;
	private LignesCommandeTableModel lignesCommandeTableModel;

	private CollectionLignesCommande collectionLignesCommande;
	private JButton btnAjouter;
	private JButton btnEditer;
	private JButton btnSupprimer;

	/**
	 * Create the frame.
	 */
	public ListeLignesCommandeFrame(CollectionLignesCommande collectionLignesCommande, boolean isPaye,
			CommandesWindowListener listener) {
		this.collectionLignesCommande = collectionLignesCommande;

		setTitle("Gestion des produits");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		lignesCommandeTableModel = new LignesCommandeTableModel(this.collectionLignesCommande);
		table = new JTable(lignesCommandeTableModel);

		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setAutoCreateColumnsFromModel(true);
		table.setAutoCreateRowSorter(true);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		contentPane.add(scrollPane, BorderLayout.CENTER);

		// La commande n'est pas pay�e, on peut afficher les �ditions de lignes de
		// commande
		if (!isPaye) {
			JPanel panel = new JPanel();
			contentPane.add(panel, BorderLayout.SOUTH);

			btnAjouter = new JButton("Ajouter");
			btnAjouter.addActionListener(this);
			panel.add(btnAjouter);

			btnEditer = new JButton("Editer");
			btnEditer.addActionListener(this);
			panel.add(btnEditer);

			btnSupprimer = new JButton("Supprimer");
			btnSupprimer.addActionListener(this);
			panel.add(btnSupprimer);
		}

		this.addWindowListener(listener);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.getBtnAjouter())
			this.ajouterLigneCommandeAction();
		else if (e.getSource() == this.getBtnEditer())
			this.modifierLigneCommandeAction();
		else if (e.getSource() == this.getBtnSupprimer())
			this.supprimerLigneCommandeAction();
	}

	private void ajouterLigneCommandeAction() {
		this.ajouterLigneCommandeAction(null);
	}

	private void ajouterLigneCommandeAction(FormulaireLigneCommande formulaireLigneCommande) {

		if (formulaireLigneCommande == null)
			formulaireLigneCommande = new FormulaireLigneCommande();

		int resultat = JOptionPane.showConfirmDialog(this, formulaireLigneCommande, "Ajout d'une ligne de commande",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (resultat == JOptionPane.OK_OPTION) {
			String quantite = formulaireLigneCommande.getChampQuantite().getText().trim();
			
			if(Integer.parseInt(quantite) == 0)
				return;

			// Champs corrects ?
			if (quantite.equals("")) {
				JOptionPane.showMessageDialog(this, "La quantit� est obligatoire", "Erreur", JOptionPane.ERROR_MESSAGE);
				this.ajouterLigneCommandeAction(formulaireLigneCommande);
				return;
			} else {
				Produit produit = (Produit) formulaireLigneCommande.getComboBoxProduit().getSelectedItem();
				LigneCommande lc = new LigneCommande(produit, Integer.parseInt(quantite),
						this.collectionLignesCommande.getCommande());

				boolean resultatAjout = this.collectionLignesCommande.ajouter(lc);
				if (!resultatAjout) {
					JOptionPane.showMessageDialog(this, "Erreur lors de l'ajout de la ligne de commande.", "Erreur",
							JOptionPane.ERROR_MESSAGE);
				}

				this.lignesCommandeTableModel.fireTableDataChanged();
			}
		}
	}

	private void modifierLigneCommandeAction() {
		this.modifierLigneCommandeAction(null, null);
	}

	private void modifierLigneCommandeAction(FormulaireLigneCommande formulaireLigneCommande,
			LigneCommande ligneCommande) {
		if (formulaireLigneCommande == null) {
			if (this.table.getSelectedRow() == -1)
				return;

			ligneCommande = this.collectionLignesCommande.getListeLignesCommande()
					.get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));
			formulaireLigneCommande = new FormulaireLigneCommande(ligneCommande.getProduit(),
					Integer.toString(ligneCommande.getQuantite()));
		}

		int resultat = JOptionPane.showConfirmDialog(this, formulaireLigneCommande, "Modification d'un produit",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (resultat == JOptionPane.OK_OPTION) {
			String quantite = formulaireLigneCommande.getChampQuantite().getText().trim();
			// Champs corrects ?
			if (quantite.equals("")) {
				JOptionPane.showMessageDialog(this, "La quantit� est obligatoire", "Erreur", JOptionPane.ERROR_MESSAGE);
				this.modifierLigneCommandeAction(formulaireLigneCommande, ligneCommande);
				return;
			} else {
								
				ligneCommande.setProduit((Produit) formulaireLigneCommande.getComboBoxProduit().getSelectedItem());
				ligneCommande.setQuantite(Integer.parseInt(quantite));

				boolean resultatModification = Integer.parseInt(quantite) == 0 ? 
						this.collectionLignesCommande.supprimer(ligneCommande) : 
							this.collectionLignesCommande.modifier(ligneCommande);
				
				if (!resultatModification) {
					JOptionPane.showMessageDialog(this, "Erreur lors de la modification de la ligne de commande.",
							"Erreur", JOptionPane.ERROR_MESSAGE);
				}

				this.lignesCommandeTableModel.fireTableDataChanged();
			}
		}
	}

	private void supprimerLigneCommandeAction() {
		if (this.table.getSelectedRow() == -1)
			return;

		LigneCommande ligneCommande = this.collectionLignesCommande.getListeLignesCommande()
				.get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));

		boolean resultatSuppression = this.collectionLignesCommande.supprimer(ligneCommande);
		if (!resultatSuppression) {
			JOptionPane.showMessageDialog(this, "Erreur lors de la suppression de la ligne de commande.", "Erreur",
					JOptionPane.ERROR_MESSAGE);
		}

		this.lignesCommandeTableModel.fireTableDataChanged();
	}

	protected JButton getBtnAjouter() {
		return btnAjouter;
	}

	public JButton getBtnEditer() {
		return btnEditer;
	}

	public JButton getBtnSupprimer() {
		return btnSupprimer;
	}

}
