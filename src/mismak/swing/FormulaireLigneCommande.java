package mismak.swing;

import java.awt.GridLayout;
import java.text.ParseException;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.MaskFormatter;

import mismak.model.CollectionProduit;
import mismak.model.Produit;

public class FormulaireLigneCommande extends JPanel {

	private static final long serialVersionUID = 181454947375235937L;
	private JComboBox<Produit> comboBoxProduit;
	private JLabel lblQuantite;
	private JFormattedTextField champQuantite;
	private MaskFormatter mask;

	public FormulaireLigneCommande() {
		super(new GridLayout(0,1));
				
		JLabel lblProduit = new JLabel("Produit :");
		add(lblProduit);
				
		// Récupération de la liste des produits non archives
		CollectionProduit cp = new CollectionProduit();
				
		comboBoxProduit = new JComboBox<Produit>(new Vector<Produit>(cp.getListeProduits()));
		add(comboBoxProduit);
		
		lblQuantite = new JLabel("Quantit\u00E9 :");
		add(lblQuantite);
		
		try {
			mask = new MaskFormatter("###");
			mask.setPlaceholderCharacter('0');
			this.champQuantite = new JFormattedTextField(mask);			
			this.add(this.champQuantite);		
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public FormulaireLigneCommande(Produit produit, String quantite) {
		this();
		this.champQuantite.setText(this.padLeftZeros(quantite, 3));
		this.comboBoxProduit.setSelectedItem(produit);
	}
	
	public JFormattedTextField getChampQuantite() {
		return champQuantite;
	}
	
	public JComboBox<Produit> getComboBoxProduit() {
		return comboBoxProduit;
	}
	
	private String padLeftZeros(String inputString, int length) {
	    if (inputString.length() >= length) {
	        return inputString;
	    }
	    StringBuilder sb = new StringBuilder();
	    while (sb.length() < length - inputString.length()) {
	        sb.append('0');
	    }
	    sb.append(inputString);

	    return sb.toString();
	}
}
