package mismak.swing;

import java.awt.GridLayout;
import java.text.ParseException;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

public class FormulaireProduit extends JPanel {

	private static final long serialVersionUID = 181454947375235937L;

	private JTextField champReference;	
	private JTextField champLibelle;
	private JFormattedTextField champPrix;
	private MaskFormatter mask;
	
	public FormulaireProduit() {
		super(new GridLayout(0,1));
				
		this.add(new JLabel("R\u00E9f\u00E9rence:"));
		this.champReference = new JTextField();
		this.add(this.champReference);
		
		this.add(new JLabel("Libell\u00E9:"));
		this.champLibelle = new JTextField();
		this.add(this.champLibelle);
		
		this.add(new JLabel("Prix HT:"));
		
		try {
			mask = new MaskFormatter("######.##");
			mask.setPlaceholderCharacter('0');
			this.champPrix = new JFormattedTextField(mask);			
			this.add(this.champPrix);		
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	}
	
	public FormulaireProduit(String reference, String libelle, String prix) {
		this();
		this.champReference.setText(reference);
		this.champPrix.setText(this.padLeftZeros(prix, 9));		
		this.champLibelle.setText(libelle);
	}
	
	private String padLeftZeros(String inputString, int length) {
	    if (inputString.length() >= length) {
	        return inputString;
	    }
	    StringBuilder sb = new StringBuilder();
	    while (sb.length() < length - inputString.length()) {
	        sb.append('0');
	    }
	    sb.append(inputString);

	    return sb.toString();
	}
	
	public JTextField getChampReference() {
		return champReference;
	}

	public JTextField getChampLibelle() {
		return champLibelle;
	}

	public JTextField getChampPrix() {
		return champPrix;
	}
}
