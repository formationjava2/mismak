package mismak.swing;

import java.awt.GridLayout;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Vector;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import mismak.model.Client;
import mismak.model.CollectionClient;

import javax.swing.JComboBox;

public class FormulaireCommande extends JPanel {

	private static final long serialVersionUID = 181454947375235937L;

	private JFormattedTextField champDate;
	private JComboBox<Client> comboBoxClient;
	
	public FormulaireCommande() {
		super(new GridLayout(0,1));
						
		this.add(new JLabel("Date :"));
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		this.champDate = new JFormattedTextField(format);
		this.add(this.champDate);
		
		JLabel lblClient = new JLabel("Client :");
		add(lblClient);
				
		// Récupération de la liste des clients non archives
		CollectionClient cc = new CollectionClient();
				
		comboBoxClient = new JComboBox<Client>(new Vector<Client>(cc.getListeClients()));
		add(comboBoxClient);
				
	}
	
	public FormulaireCommande(Client client, String date) {
		this();
		this.champDate.setText(date);
		this.comboBoxClient.setSelectedItem(client);
	}
	
	public JTextField getChampDate() {
		return champDate;
	}
	
	public JComboBox<Client> getComboBoxClient() {
		return comboBoxClient;
	}
}
