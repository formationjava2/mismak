package mismak.swing;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import mismak.model.CollectionCommande;
import mismak.swing.tablemodel.CommandesTableModel;

public class CommandesWindowListener extends WindowAdapter {

	private CommandesTableModel commandesTableModel;
	private CollectionCommande collectionCommandes;
	
	public CommandesWindowListener(CommandesTableModel commandesTableModel, CollectionCommande collectionCommandes) {
		super();
		this.commandesTableModel = commandesTableModel;
		this.collectionCommandes = collectionCommandes;
	}
	
	@Override
	public void windowClosed(WindowEvent e) {
		this.collectionCommandes.refresh();
		this.commandesTableModel.fireTableDataChanged();
	}

}
