package mismak.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import mismak.dao.model.LignesCommandeDAO;
import mismak.model.LigneCommande;
import mismak.model.Produit;

public class LignesCommandeDAOImpl extends LignesCommandeDAO {

	public boolean create(LigneCommande ligne) {
		try {
			PreparedStatement prepare = this.connect
					.prepareStatement("INSERT INTO ligne_commande(commande_id, produit_id, quantite) VALUES(?, ?, ?)");
			prepare.setInt(1, ligne.getCommande().getId());
			prepare.setInt(2, ligne.getProduit().getId());
			prepare.setInt(3, ligne.getQuantite());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean update(LigneCommande ligne) {
		try {
			PreparedStatement prepare = this.connect
					.prepareStatement("UPDATE ligne_commande SET produit_id=?, quantite=? where id=?");
			prepare.setInt(1, ligne.getProduit().getId());
			prepare.setInt(2, ligne.getQuantite());
			prepare.setInt(3, ligne.getId());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean delete(LigneCommande ligne) {
		try {
			PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM ligne_commande where id=?");
			prepare.setInt(1, ligne.getId());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public ArrayList<LigneCommande> findByCommandeId(int commandeId) {
		ArrayList<LigneCommande> resultat = new ArrayList<LigneCommande>();

		String requete = "SELECT lc.*, pr.* FROM ligne_commande lc INNER JOIN produit pr ON lc.produit_id = pr.id INNER JOIN commande co ON lc.commande_id = co.id WHERE co.id = ?";

		try {
			PreparedStatement prepare = this.connect.prepareStatement(requete);
			prepare.setInt(1, commandeId);
			ResultSet rs = prepare.executeQuery();
			while (rs.next()) {
				resultat.add(LignesCommandeDAOImpl.map(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultat;
	}

	@Override
	public Map<Integer, ArrayList<LigneCommande>> findByClientIdGroupedByCommandeId(int clientId) {

		Map<Integer, ArrayList<LigneCommande>> resultat = new LinkedHashMap<Integer, ArrayList<LigneCommande>>();

		String requete = "SELECT lc.*, pr.*, co.id FROM ligne_commande lc "+
				" INNER JOIN produit pr ON lc.produit_id = pr.id " +
				" INNER JOIN commande co ON lc.commande_id = co.id "+ 
				" INNER JOIN client cl ON co.client_id = cl.id " +
				" WHERE cl.id = ?";

		try {
			PreparedStatement prepare = this.connect.prepareStatement(requete);
			prepare.setInt(1, clientId);
			ResultSet rs = prepare.executeQuery();
			while (rs.next()) {
				LigneCommande lc = LignesCommandeDAOImpl.map(rs);
				if(resultat.get(rs.getInt("co.id")) == null) {
					resultat.put(rs.getInt("co.id"), new ArrayList<LigneCommande>());
				}
				resultat.get(rs.getInt("co.id")).add(lc);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultat;
	}

	private static LigneCommande map(ResultSet rs) throws SQLException {
		Produit produit = new Produit();
		produit.setId(rs.getInt("pr.id"));
		produit.setLibelle(rs.getString("pr.libelle"));
		produit.setReference(rs.getString("pr.reference"));
		produit.setPrixHT(rs.getBigDecimal("pr.prix"));

		LigneCommande ligne = new LigneCommande();
		ligne.setProduit(produit);
		ligne.setId(rs.getInt("lc.id"));
		ligne.setQuantite(rs.getInt("lc.quantite"));

		return ligne;
	}
}
