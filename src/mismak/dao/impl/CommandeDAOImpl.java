package mismak.dao.impl;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import mismak.dao.model.CommandeDAO;
import mismak.model.Client;
import mismak.model.Commande;
import mismak.model.Produit;

public class CommandeDAOImpl extends CommandeDAO {

	public boolean create(Commande commande) {
		try {
			PreparedStatement prepare = this.connect
					.prepareStatement("INSERT INTO commande(client_id, date, paye) VALUES(?, ?, ?)");
			prepare.setInt(1, commande.getClient().getId());
			prepare.setDate(2, new Date(commande.getDate().getTime()));
			prepare.setBoolean(3, commande.isPaye());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean update(Commande commande) {
		try {
			PreparedStatement prepare = this.connect
					.prepareStatement("UPDATE commande SET client_id=?, date=?, paye=? where id=?");
			prepare.setInt(1, commande.getClient().getId());
			prepare.setDate(2, new Date(commande.getDate().getTime()));
			prepare.setBoolean(3, commande.isPaye());
			prepare.setInt(4, commande.getId());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public ArrayList<Commande> findAll(boolean withTotalHT) {
		ArrayList<Commande> resultat = new ArrayList<Commande>();

		String requete;

		if (withTotalHT)
			requete = "SELECT co.*, cl.*, SUM(lc.quantite * pr.prix) as total_ht FROM commande co "
					+ "LEFT JOIN ligne_commande lc ON lc.commande_id = co.id "
					+ "LEFT JOIN produit pr ON lc.produit_id = pr.id " + "INNER JOIN client cl ON co.client_id = cl.id "
					+ "GROUP BY co.id";
		else
			requete = "SELECT co.*, cl.* FROM commande co INNER JOIN client cl ON co.client_id = cl.id";

		try {
			ResultSet rs = this.connect.createStatement().executeQuery(requete);
			while (rs.next()) {
				resultat.add(CommandeDAOImpl.mapWithClient(rs, withTotalHT));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return resultat;
	}

	public boolean delete(Commande commande) {
		try {
			PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM commande where id=?");
			prepare.setInt(1, commande.getId());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	@Override
	public ArrayList<Commande> findByClient(Client client, boolean withTotalHT) {
		ArrayList<Commande> resultat = new ArrayList<Commande>();
		try {
			String requete;
			if (withTotalHT)
				requete = "SELECT co.*, cl.*, SUM(lc.quantite * pr.prix) as total_ht FROM commande co "
						+ "LEFT JOIN ligne_commande lc ON lc.commande_id = co.id "
						+ "LEFT JOIN produit pr ON lc.produit_id = pr.id "
						+ "INNER JOIN client cl ON co.client_id = cl.id " + "WHERE co.client_id = ? "
						+ "GROUP BY co.id";
			else
				requete = "SELECT * FROM commande WHERE client_id=?";

			PreparedStatement prepare = this.connect.prepareStatement(requete);
			prepare.setInt(1, client.getId());
			ResultSet rs = prepare.executeQuery();

			while (rs.next()) {
				resultat.add(CommandeDAOImpl.mapWithClient(rs, withTotalHT));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultat;
	}

	@Override
	public ArrayList<Commande> findByProduit(Produit produit) {
		ArrayList<Commande> resultat = new ArrayList<Commande>();
		try {
			PreparedStatement prepare = this.connect.prepareStatement(
					"SELECT * FROM commande c INNER JOIN ligne_commande lc ON c.id = lc.commande_id WHERE lc.produit_id=?");
			prepare.setInt(1, produit.getId());
			ResultSet rs = prepare.executeQuery();

			while (rs.next()) {
				resultat.add(CommandeDAOImpl.map(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultat;
	}

	private static Commande map(ResultSet rs) throws SQLException {
		Commande commande = new Commande();
		commande.setId(rs.getInt("co.id"));
		commande.setDate(rs.getDate("co.date"));
		commande.setPaye(rs.getBoolean("co.paye"));

		return commande;
	}

	private static Commande mapWithClient(ResultSet rs, boolean totalHT) throws SQLException {
		Commande commande = new Commande();
		commande.setId(rs.getInt("co.id"));
		commande.setDate(rs.getDate("co.date"));
		commande.setPaye(rs.getBoolean("co.paye"));

		if (totalHT)
			commande.setTotalHT(rs.getBigDecimal("total_ht") == null ? BigDecimal.ZERO : rs.getBigDecimal("total_ht"));

		Client client = new Client();
		client.setId(rs.getInt("cl.id"));
		client.setNom(rs.getString("cl.nom"));
		client.setPrenom(rs.getString("cl.prenom"));
		client.setDateNaissance(rs.getDate("cl.date_naissance"));
		client.setEmail(rs.getString("cl.email"));
		client.setArchive(rs.getBoolean("cl.archive"));

		commande.setClient(client);

		return commande;
	}

	@Override
	public Commande findByClientAndCommandeId(Client client, int idCommandeRecherche, boolean withTotalHT) {
		try {
			String requete;
			if (withTotalHT)
				requete = "SELECT co.*, cl.*, SUM(lc.quantite * pr.prix) as total_ht FROM commande co "
						+ "LEFT JOIN ligne_commande lc ON lc.commande_id = co.id "
						+ "LEFT JOIN produit pr ON lc.produit_id = pr.id "
						+ "INNER JOIN client cl ON co.client_id = cl.id " + "WHERE co.client_id = ? AND co.id = ? "
						+ "GROUP BY co.id";
			else
				requete = "SELECT * FROM commande WHERE client_id=? AND co.id = ?";

			PreparedStatement prepare = this.connect.prepareStatement(requete);
			prepare.setInt(1, client.getId());
			prepare.setInt(2, idCommandeRecherche);
			ResultSet rs = prepare.executeQuery();

			if (rs.next()) {
				return CommandeDAOImpl.mapWithClient(rs, withTotalHT);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Commande findByCommandeId(int idCommandeRecherche, boolean withTotalHT) {
		
		String requete;

		if (withTotalHT)
			requete = "SELECT co.*, cl.*, SUM(lc.quantite * pr.prix) as total_ht FROM commande co "
					+ "LEFT JOIN ligne_commande lc ON lc.commande_id = co.id "
					+ "LEFT JOIN produit pr ON lc.produit_id = pr.id " + "INNER JOIN client cl ON co.client_id = cl.id "
					+ "WHERE co.id=? "
					+ "GROUP BY co.id";
		else
			requete = "SELECT co.*, cl.* FROM commande co INNER JOIN client cl ON co.client_id = cl.id WHERE co.id = ?";

		try {
			PreparedStatement prepare = this.connect.prepareStatement(requete);
			prepare.setInt(1, idCommandeRecherche);
			ResultSet rs = prepare.executeQuery();
			if (rs.next()) {
				return CommandeDAOImpl.mapWithClient(rs, withTotalHT);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public BigDecimal getCATotal() {		
		String requete = "SELECT SUM(lc.quantite * p.prix) as total "
				+ "FROM ligne_commande lc " 
				+ "INNER JOIN produit p ON lc.produit_id = p.id";
		BigDecimal result = null;
		
		try {
			ResultSet rs = this.connect.createStatement().executeQuery(requete);
			result = rs.next() ? rs.getBigDecimal("total") : null;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}

}
