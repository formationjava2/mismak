package mismak.dao.impl;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import mismak.dao.model.ClientDAO;
import mismak.model.Client;

public class ClientDAOImpl extends ClientDAO {

	public boolean create(Client client) {
		try {
			PreparedStatement prepare = this.connect
					.prepareStatement("INSERT INTO client(nom, prenom, date_naissance, email) VALUES(?, ?, ?, ?)");
			prepare.setString(1, client.getNom());
			prepare.setString(2, client.getPrenom());
			prepare.setDate(3, new Date(client.getDateNaissance().getTime()));
			prepare.setString(4, client.getEmail());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean update(Client client) {
		try {
			PreparedStatement prepare = this.connect
					.prepareStatement("UPDATE client SET nom=?, prenom=?, date_naissance=?, email=?, archive=? where id=?");
			prepare.setString(1, client.getNom());
			prepare.setString(2, client.getPrenom());
			prepare.setDate(3, new Date(client.getDateNaissance().getTime()));
			prepare.setString(4, client.getEmail());
			prepare.setBoolean(5, client.isArchive());
			prepare.setInt(6, client.getId());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public ArrayList<Client> findAll() {
		return this.findAll(false);
	}
	
	public ArrayList<Client> findAll(boolean archive) {
		ArrayList<Client> resultat = new ArrayList<Client>();
		try {
			ResultSet rs = this.connect.createStatement().executeQuery(archive ? "SELECT * FROM client" : "SELECT * FROM client WHERE archive = 0");
			while (rs.next()) {
				resultat.add(ClientDAOImpl.map(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultat;
	}
	
	public boolean delete(Client client) {
		try {
			PreparedStatement prepare = this.connect
					.prepareStatement("DELETE FROM client where id=?");
			prepare.setInt(1, client.getId());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	private static Client map(ResultSet rs) throws SQLException {
		Client client = new Client();
		client.setId(rs.getInt("id"));
		client.setNom(rs.getString("nom"));
		client.setPrenom(rs.getString("prenom"));
		client.setDateNaissance(rs.getDate("date_naissance"));
		client.setEmail(rs.getString("email"));
		client.setArchive(rs.getBoolean("archive"));
		return client;
	}

	@Override
	public Map<Client, BigDecimal> getCAParClient() {
		Map<Client, BigDecimal> resultat = new LinkedHashMap<Client, BigDecimal>();
		String requete = "SELECT cl.*, IFNULL(SUM(lc.quantite * p.prix), 0) as ca "
				+ "	FROM client cl "
				+ "	LEFT JOIN commande co ON cl.id = co.client_id "
				+ "	LEFT JOIN ligne_commande lc ON co.id = lc.commande_id "
				+ "	LEFT JOIN produit p ON p.id = lc.produit_id "
				+ "	GROUP BY cl.id "
				+ " ORDER BY ca DESC";
		
		try {
			ResultSet rs = this.connect.createStatement().executeQuery(requete);
			while (rs.next()) {
				resultat.put(ClientDAOImpl.map(rs), rs.getBigDecimal("ca"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultat;
	}

	@Override
	public Client findByClientId(int idClientRecherche) {
		String requete = "SELECT * FROM client WHERE id = ?";

		try {
			PreparedStatement prepare = this.connect.prepareStatement(requete);
			prepare.setInt(1, idClientRecherche);
			ResultSet rs = prepare.executeQuery();
			if (rs.next()) {
				return ClientDAOImpl.map(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

}
