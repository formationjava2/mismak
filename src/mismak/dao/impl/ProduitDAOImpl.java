package mismak.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import mismak.dao.model.ProduitDAO;
import mismak.model.Produit;

public class ProduitDAOImpl extends ProduitDAO {

	public boolean create(Produit produit) {
		try {
			PreparedStatement prepare = this.connect
					.prepareStatement("INSERT INTO produit(reference, libelle, prix) VALUES(?, ?, ?)");
			prepare.setString(1, produit.getReference());
			prepare.setString(2, produit.getLibelle());
			prepare.setBigDecimal(3, produit.getPrixHT());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean update(Produit produit) {
		try {
			PreparedStatement prepare = this.connect
					.prepareStatement("UPDATE produit SET reference=?, libelle=?, prix=?, archive=? where id=?");
			prepare.setString(1, produit.getReference());
			prepare.setString(2, produit.getLibelle());
			prepare.setBigDecimal(3, produit.getPrixHT());
			prepare.setBoolean(4, produit.isArchive());
			prepare.setInt(5, produit.getId());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public ArrayList<Produit> findAll() {
		return this.findAll(false);
	}
	
	public ArrayList<Produit> findAll(boolean archive) {
		ArrayList<Produit> resultat = new ArrayList<Produit>();
		try {
			ResultSet rs = this.connect.createStatement().executeQuery(archive ? "SELECT * FROM produit" : "SELECT * FROM produit WHERE archive = 0");
			while (rs.next()) {
				resultat.add(ProduitDAOImpl.map(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultat;
	}
	
	public boolean delete(Produit produit) {
		try {
			PreparedStatement prepare = this.connect
					.prepareStatement("DELETE FROM produit where id=?");
			prepare.setInt(1, produit.getId());
			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	@Override
	public Map<Produit, Integer> getPlusVendus(int nombre) {
		
		Map<Produit, Integer> resultat = new LinkedHashMap<Produit, Integer>();
		try {
			String requete = "SELECT p.*, IFNULL(SUM(lc.quantite), 0) as total "
					+ "FROM produit p "
					+ "LEFT JOIN ligne_commande lc ON lc.produit_id = p.id "
					+ "GROUP BY p.id "
					+ "ORDER BY total DESC "
					+ "LIMIT ?";
			
			PreparedStatement prepare = this.connect
					.prepareStatement(requete);
			prepare.setInt(1, nombre);
			ResultSet rs = prepare.executeQuery();
			
			while (rs.next()) {
				resultat.put(ProduitDAOImpl.map(rs), rs.getInt("total"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultat;
	}

	@Override
	public Map<Produit, Integer> getMoinsVendus(int nombre) {
		Map<Produit, Integer> resultat = new LinkedHashMap<Produit, Integer>();
		try {
			String requete = "SELECT p.*, IFNULL(SUM(lc.quantite), 0) as total "
					+ "FROM produit p "
					+ "LEFT JOIN ligne_commande lc ON lc.produit_id = p.id "
					+ "GROUP BY p.id "
					+ "ORDER BY total ASC "
					+ "LIMIT ?";
			
			PreparedStatement prepare = this.connect
					.prepareStatement(requete);
			prepare.setInt(1, nombre);
			ResultSet rs = prepare.executeQuery();
			
			while (rs.next()) {
				resultat.put(ProduitDAOImpl.map(rs), rs.getInt("total"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultat;
	}

	private static Produit map(ResultSet rs) throws SQLException {
		Produit produit = new Produit();
		produit.setId(rs.getInt("id"));
		produit.setReference(rs.getString("reference"));
		produit.setLibelle(rs.getString("libelle"));
		produit.setPrixHT(rs.getBigDecimal("prix"));
		produit.setArchive(rs.getBoolean("archive"));
		return produit;
	}

	

}
