package mismak.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DAOFactory {
	private static final String URL = "jdbc:mysql://localhost:3306/mismak";
	private static final String USER = "mismak";
	private static final String PASSWORD = "mismak";
	private static final String DRIVER_JDBC = "com.mysql.cj.jdbc.Driver";
    private static Connection connection;

	public static Connection getInstance() {

		if(connection == null) {
			try {
				Class.forName(DAOFactory.DRIVER_JDBC);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			try {
				connection = DriverManager.getConnection(URL, USER, PASSWORD);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return connection;		
	}
}
