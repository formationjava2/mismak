package mismak.dao.model;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Map;

import mismak.dao.DAOFactory;
import mismak.model.Produit;

public abstract class ProduitDAO {
    public Connection connect = DAOFactory.getInstance();

	public abstract boolean create(Produit produit);
	public abstract boolean update(Produit produit);
	public abstract boolean delete(Produit produit);
	public abstract ArrayList<Produit> findAll();
	public abstract ArrayList<Produit> findAll(boolean archive);
	public abstract Map<Produit, Integer> getPlusVendus(int nombre);
	public abstract Map<Produit, Integer> getMoinsVendus(int nombre);
}
