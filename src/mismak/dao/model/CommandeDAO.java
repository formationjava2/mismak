package mismak.dao.model;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;

import mismak.dao.DAOFactory;
import mismak.model.Client;
import mismak.model.Commande;
import mismak.model.Produit;

public abstract class CommandeDAO {
    public Connection connect = DAOFactory.getInstance();

    public abstract boolean create(Commande commande);
	public abstract boolean update(Commande commande);
	public abstract boolean delete(Commande commande);
	public abstract ArrayList<Commande> findAll(boolean withTotalHT);
	public abstract ArrayList<Commande> findByClient(Client client, boolean withTotalHT);
	public abstract ArrayList<Commande> findByProduit(Produit produit);
	public abstract Commande findByClientAndCommandeId(Client client, int idCommandeRecherche, boolean withTotalHT);
	public abstract Commande findByCommandeId(int idCommandeRecherche, boolean withTotalHT);
	public abstract BigDecimal getCATotal();
}
