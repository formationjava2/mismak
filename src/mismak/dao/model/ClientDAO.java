package mismak.dao.model;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Map;

import mismak.dao.DAOFactory;
import mismak.model.Client;

public abstract class ClientDAO {
    public Connection connect = DAOFactory.getInstance();

	public abstract boolean create(Client client);
	public abstract boolean update(Client client);
	public abstract boolean delete(Client client);
	public abstract ArrayList<Client> findAll();
	public abstract ArrayList<Client> findAll(boolean archive);
	public abstract Map<Client, BigDecimal> getCAParClient();
	public abstract Client findByClientId(int idClientRecherche);
}
