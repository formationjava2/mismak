package mismak.dao.model;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Map;

import mismak.dao.DAOFactory;
import mismak.model.LigneCommande;

public abstract class LignesCommandeDAO {
    public Connection connect = DAOFactory.getInstance();

	public abstract boolean create(LigneCommande ligne);
	public abstract boolean update(LigneCommande ligne);
	public abstract boolean delete(LigneCommande ligne);
	public abstract ArrayList<LigneCommande> findByCommandeId(int commandeId);
	public abstract Map<Integer, ArrayList<LigneCommande>> findByClientIdGroupedByCommandeId(int clientId);
}
