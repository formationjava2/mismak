package mismak;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import mismak.pdf.Statistiques;
import mismak.swing.ListeClientsFrame;
import mismak.swing.ListeCommandesFrame;
import mismak.swing.ListeProduitsFrame;

public class Mismak implements ActionListener {

	private JFrame frmMismak;
	private JButton btnClients;
	private ListeClientsFrame clientsFrame;
	private ListeProduitsFrame produitsFrame;
	private ListeCommandesFrame commandesFrame;

	private JButton btnProduits;
	private JButton btnCommandes;
	private JButton btnStatistiques;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mismak window = new Mismak();
					window.frmMismak.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Mismak() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMismak = new JFrame();
		frmMismak.setResizable(false);
		frmMismak.setTitle("Mismak");
		frmMismak.setBounds(100, 100, 240, 196);
		frmMismak.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		btnClients = new JButton("Gestion des clients");
		btnClients.addActionListener(this);
		frmMismak.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		frmMismak.getContentPane().add(btnClients);
		
		btnProduits = new JButton("Gestion des produits");
		btnProduits.addActionListener(this);
		frmMismak.getContentPane().add(btnProduits);
		
		btnCommandes = new JButton("Gestion des commandes");
		btnCommandes.addActionListener(this);
		frmMismak.getContentPane().add(btnCommandes);
		
		btnStatistiques = new JButton("Editer les statistiques");
		btnStatistiques.addActionListener(this);
		frmMismak.getContentPane().add(btnStatistiques);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.btnClients) {
			if(clientsFrame == null) {				
				this.clientsFrame = new ListeClientsFrame();
			}
			clientsFrame.setVisible(true);
		}
		else if(e.getSource() == this.btnProduits) {
			if(produitsFrame == null) {				
				this.produitsFrame = new ListeProduitsFrame();
			}
			produitsFrame.setVisible(true);
		}
		else if(e.getSource() == this.btnCommandes) {
			if(commandesFrame == null) {				
				this.commandesFrame = new ListeCommandesFrame(null);
			}
			commandesFrame.setVisible(true);
		}
		else if(e.getSource() == this.btnStatistiques) {

			String fichier = Statistiques.genererPDF();
			if(fichier != null) {
				JOptionPane.showMessageDialog(frmMismak, "Le fichier " + fichier + " �t� g�n�r�.", "Statistiques",
						JOptionPane.INFORMATION_MESSAGE);
			}
			else {
				JOptionPane.showMessageDialog(frmMismak, "Erreur lors de la cr�ation du fichier de statistiques.", "Erreur",
						JOptionPane.ERROR_MESSAGE);
			}
			
		}
	}

	protected JButton getBtnProduits() {
		return btnProduits;
	}
	protected JButton getBtnCommandes() {
		return btnCommandes;
	}
	protected JButton getBtnStatistiques() {
		return btnStatistiques;
	}
}
