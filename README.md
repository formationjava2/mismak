# Projet Mismak

Pour lancer le programme :
- Exécuter le script `export.sql` sur la base MySQL. Ce script créé une base exemple
- Configurer le fichier `/mismak/src/mismak/dao/DAOFactory.java` avec les bonnes informations de connexion à la base
- La classe principale est `/mismak/src/mismak/Mismak.java`
